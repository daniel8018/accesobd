package rivera.daniel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conection {

    protected Connection contacto;

    private final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private final String DB_URL = "jdbc:sqlserver://localhost:1433;database=Proyecto";

    //Credenciales de la base de datos
    private final String USER = "sa";
    private final String PASS = "1234";

    public void conectar() throws Exception {
      try{
          contacto = DriverManager.getConnection(DB_URL,USER,PASS);
          Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

      }catch (Exception e){
         throw e;
      }
    }//FIN CONECTAR


    public void cerrar()throws SQLException {
      if(contacto!=null){
          if(!contacto.isClosed()){
               contacto.close();
          }
      }

    }
}
