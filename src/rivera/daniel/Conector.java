package Montero.Lizbeth.Accesobd;


import java.sql.*;

/**
 * Clase que se encarga de crear el objeto de acceso a datos, seg�n el
 * motor que se desea utilizar.
 * @author Universidad Cenfotec
 * @version 2.0
 * @since 2019
 */
public class Conector {
    /**
     * conectorBD variable est�tica de la clase de acceso a datos.
     */
    private static AccesoBD conectorBD = null;
    /**
     *
     * @param dirver
     * @param connectionStr
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static AccesoBD getConector(String dirver,String connectionStr) throws SQLException, ClassNotFoundException{
        if(conectorBD == null){
            conectorBD = new AccesoBD(dirver,connectionStr);
        }
        return conectorBD;
    }

    /**
     *
     * @param dirver
     * @param url
     * @param user
     * @param clave
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static AccesoBD getConector(String dirver,String url,String user,
                                       String clave)
            throws SQLException, ClassNotFoundException{
        if(conectorBD == null){
            conectorBD = new AccesoBD(dirver,url,user,clave);
        }
        return conectorBD;
    }




}
